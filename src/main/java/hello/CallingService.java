package hello;

import hello.bean.Configuration;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

public class CallingService {

    private HttpClient client;
    private CachingService cachingService;

    public CallingService(CachingService cachingService) {
        this.cachingService = cachingService;
        client = HttpClientBuilder.create().build();
    }

    public String getResponse(String body, Map<String, String> headers, HttpServletRequest sr, String url, Configuration configuration){
        final String key = getKey(body, headers, sr, url, configuration);
        if(configuration.getMode().equals("mock")){
            final String res = cachingService.get(key);
            if(StringUtils.isEmpty(res)){
                return cachingService.getAllValues().values().iterator().next();
            }
            return res;
        } else{
            final String result = doSoapCall(body, headers, sr, url);
            cachingService.put(key, result);
            return result;
        }
    }

    private String getKey(String body, Map<String, String> headers, HttpServletRequest sr, String url, Configuration configuration){
        String soapaction = headers.getOrDefault("soapaction", "");
        return sr.getRequestURI()+soapaction;
    }

    private String doSoapCall(String body, Map<String, String> headers, HttpServletRequest sr, String url){
        String restOfTheUrl = sr.getRequestURI();
        url += restOfTheUrl;
        String soapaction = headers.getOrDefault("soapaction", "");
        soapaction = soapaction.replace("\"", "");
        StringEntity stringEntity = new StringEntity(body, "UTF-8");
        stringEntity.setChunked(true);
        HttpPost post = new HttpPost(url);
        post.setEntity(stringEntity);
        post.addHeader("SOAPAction", soapaction);
        post.addHeader("Accept", "text/xml");
        post.addHeader("content-type", "text/xml; charset=utf-8");
        try {
            HttpResponse response = client.execute(post);
            final HttpEntity entity = response.getEntity();
            return EntityUtils.toString(entity);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
