package hello;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Context {

    @Bean("configService")
    public ConfigService getConfigService(){
        return new ConfigService();
    }

}
