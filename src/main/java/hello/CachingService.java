package hello;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CachingService {
    private ConcurrentHashMap<String, String> concurrentHashMap;

    public CachingService() {
        this.concurrentHashMap = new ConcurrentHashMap<>();
    }

    public void put(String key, String value){
        concurrentHashMap.put(key, value);
    }

    public String get(String key){
        return concurrentHashMap.get(key);
    }

    public Map<String, String> getAllValues(){
        return concurrentHashMap;
    }

}
