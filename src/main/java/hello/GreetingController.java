package hello;

import hello.bean.Configuration;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import sun.misc.ObjectInputFilter;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RestController
public class GreetingController {
    private ScheduledExecutorService executorService = Executors.newScheduledThreadPool(50);
    @Autowired
    private ConfigService configService;
    private CachingService cachingService;
    private CallingService callingService;

    public GreetingController(ConfigService configService) {
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("res.json");
        cachingService = new CachingService();
        callingService = new CallingService(cachingService);
    }

      @RequestMapping("/**")
      public DeferredResult<ResponseEntity<?>> index(@RequestBody String body, @RequestHeader Map<String, String> headers, HttpServletRequest sr) {
          DeferredResult<ResponseEntity<?>> output = new DeferredResult<>();
          final Configuration configuration = configService.getConfiguration();
          String result = callingService.getResponse(body, headers, sr, configuration.getProxyForwardUrl(), configuration);
          Runnable cmd = () -> {
              output.setResult(ResponseEntity.ok(result));
          };
          long delay = configuration.getMockLatency();
          executorService.schedule(cmd, delay, TimeUnit.MILLISECONDS);
          return output;

      }

    @RequestMapping("/cache")
    public Map<String, String> getCache(){
        final HashMap<String, String> map = new HashMap<>(cachingService.getAllValues());
        map.replaceAll((k,v)->  v.substring(0, Math.min(v.length(), 50)));
        return map;
    }


   /* @RequestMapping("/**")
    private String proxy(@RequestBody String body, @RequestHeader Map<String, String> headers, HttpServletRequest sr) {
        String url = "https://nodeD1.test.webservices.amadeus.com";
        return doSoapCall(body, headers, sr, url);

    }*/



}