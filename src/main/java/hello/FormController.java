package hello;

import hello.bean.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class FormController {
    @Autowired
    private ConfigService configService;

    @GetMapping("/config")
    public String sendForm(Configuration user) {
        return "config";
    }

    @PostMapping("/config")
    public String processForm(Configuration user) {
        configService.saveConfiguation(user);
        return "showMessage";
    }

    @ModelAttribute("contentTypes")
    public String[] getContentTypes() {
        return new String[]{
                "application/json", "application/xml"
        };
    }

    @ModelAttribute("modes")
    public String[] getModes() {
        return new String[]{
                "proxy","mock"
        };
    }
}
