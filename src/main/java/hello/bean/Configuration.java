package hello.bean;


public class Configuration {
    private String mode;
    private String proxyForwardUrl;
    private boolean matchBody;
    private String contentType;
    private int mockLatency;

    public int getMockLatency() {
        return mockLatency;
    }

    public void setMockLatency(int mockLatency) {
        this.mockLatency = mockLatency;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getProxyForwardUrl() {
        return proxyForwardUrl;
    }

    public void setProxyForwardUrl(String proxyForwardUrl) {
        this.proxyForwardUrl = proxyForwardUrl;
    }

    public boolean isMatchBody() {
        return matchBody;
    }

    public void setMatchBody(boolean matchBody) {
        this.matchBody = matchBody;
    }
}