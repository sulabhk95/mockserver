package hello;

import hello.bean.Configuration;

public class ConfigService {
    private Configuration configuration;

    public Configuration getConfiguration(){
        return configuration;
    }

    public void saveConfiguation(Configuration configuration){
        this.configuration = configuration;
    }
}
